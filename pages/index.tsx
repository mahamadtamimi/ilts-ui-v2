import DefaultLayout from "@/layouts/default";
import Banner from "@/components/Banner";

export default function IndexPage() {
	return (
		<DefaultLayout>
			<Banner/>
		</DefaultLayout>
	);
}
